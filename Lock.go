package alock

import (
	"runtime"
	"sync/atomic"
	"time"
	"unsafe"
)

//Lock os a Locker
type Lock struct {
	parent    *Lock
	Timeout   time.Duration
	state     uint32
	tail      uint32
	dead      int64
	OnAllDone func()
}

//Parent returns the parent lock
func (j *Lock) Parent() *Lock {
	ptr := unsafe.Pointer(j.parent)
	return (*Lock)(atomic.LoadPointer(&ptr))
}

//CompareAndSwapParent does CAS on the parent lock
func (j *Lock) CompareAndSwapParent(oldOne, anotherNew *Lock) bool {
	j.waitForParent()
	ptr := unsafe.Pointer(j.parent)
	return atomic.CompareAndSwapPointer(&ptr, unsafe.Pointer(oldOne), unsafe.Pointer(anotherNew))
}

//New Takes a new turn in the Lock
func (j *Lock) New() *Turn {
	j.waitForParent()
	atomic.CompareAndSwapUint32(&j.state, 0, 1)
	if j.Num() > MaxLen {
		panic(ErrMaxLen)
	}
	return &Turn{j, j.incTail()}
}

//Reset resets the lock and release its waiting
//but de previous timeout remains
func (j *Lock) Reset() {
	j.waitForParent()
	atomic.StoreUint32(&j.tail, 0)
	atomic.StoreUint32(&j.state, 0)
	atomic.StoreInt64(&j.dead, 0)
}

//Wait waits for all turns come
func (j *Lock) Wait() {
	j.waitForParent()
	atomic.CompareAndSwapUint32(&j.state, 0, 1)
	for atomic.CompareAndSwapUint32(&j.state, j.tail+1, j.tail+1) == false {
		runtime.Gosched()
	}
	if j.OnAllDone != nil {
		j.OnAllDone()
	}
}

//WaitChan do Wait but in Chan form
func (j *Lock) WaitChan() chan struct{} {
	c := make(chan struct{})
	go watchLockerAndChan(j, c)
	return c
}

//Num returns num of the Locks shoud proceed
func (j *Lock) Num() uint32 {
	state, tail := atomic.LoadUint32(&j.tail), atomic.LoadUint32(&j.state)
	if tail < state {
		return tail + state - 1
	}
	return tail - state - 1
}

//Proceed proceed the Lock to next Turn
// you should call proceed in first use to fire the lock queue
func (j *Lock) Proceed() {
	j.waitForParent()
	if j.updateState(j.state) {
		j.updateDeadline()
	}
}
