package alock

//Lock acts like Mutex Lock
func (j *Lock) Lock() {
	j.New().Wait()
}

//Unlock acts like Mutex Unlock
func (j *Lock) Unlock() {
	j.Proceed()
}
