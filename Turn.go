package alock

import (
	"runtime"
	"sync/atomic"
)

var (
	uncome int32 = 0
	come   int32 = 0
	gone   int32 = 0
)

//Turn is a turn in the Lock
type Turn struct {
	lock   *Lock
	state  uint32
	status int32
}

//Wait Waits until its turn come
func (t Turn) Wait() {
	if atomic.LoadInt32(&t.status) == gone {
		panic(ErrCantWait)
	}
	state := atomic.LoadUint32(&t.lock.state)
	for state != t.state {
		t.lock.checkTimeout(state)
		state = atomic.LoadUint32(&t.lock.state)
		runtime.Gosched()
	}
	atomic.CompareAndSwapInt32(&t.status, uncome, come)
}

//Done Proceed the Lock for next turn
func (t *Turn) Done() {
	switch atomic.LoadInt32(&t.status) {
	case uncome:
		panic(ErrUncome)
	case gone:
		return
	case come:
		if !atomic.CompareAndSwapInt32(&t.status, come, gone) {
			return
		}
	}
	t.lock.proceed(t.state)
}
