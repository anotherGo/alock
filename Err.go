package alock

import "errors"

//ErrMaxLen throws when max len (maxLen) exeed for Len of a Lock
var ErrMaxLen = errors.New("alock: max len exceed")
var ErrCantWait = errors.New("alock: can not wait for this turn")
var ErrUncome = errors.New("alock: uncome turn")
var ErrUnknown = errors.New("alock: unknown error")
