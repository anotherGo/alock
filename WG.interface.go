package alock

//Add acts like WaitGroup Add
func (j *Lock) Add(delta int) {
	for i := 0; i < delta; i++ {
		j.New()
	}
}

//Done acts like WaitGroup Done
func (j *Lock) Done() {
	j.Proceed()
}
