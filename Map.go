package alock

import (
	"github.com/cornelk/hashmap"
)

//Bank perform a bank of the Locks
type mapOfLocks struct {
	hashmap *hashmap.HashMap
}

func NewMapOfLocks(size uintptr) *mapOfLocks {
	return &mapOfLocks{hashmap.New(size)}
}
func (mol *mapOfLocks) Get(key []byte) *Lock {
	lock := &Lock{}
	lock.OnAllDone()
	lock, _ = mol.hashmap.GetOrInsert(key, &Lock{})
	return lock.(*Lock)
}
