package alock

import (
	"sync/atomic"
	"time"
)

func (j *Lock) incTail() (uid uint32) {
	for uid == 0 {
		uid = atomic.AddUint32(&j.tail, 1)
	}
	return
}
func watchLockerAndChan(j *Lock, c chan struct{}) {
	j.Wait()
	close(c)
}
func (j *Lock) updateState(condState uint32) bool {
	if condState+1 == 0 {
		condState++
	}
	return atomic.CompareAndSwapUint32(&j.state, condState, condState+1)
}
func (j *Lock) waitForParent() {
	var p *Lock
	p = j.Parent()
	if p != nil {
		p.Wait()
	}
}
func (j *Lock) checkTimeout(condState uint32) {
	if j.Timeout.Nanoseconds() == 0 {
		return
	}
	dead := atomic.LoadInt64(&j.dead)
	if dead < time.Now().UnixNano() {
		j.proceed(condState)
	}
}
func (j *Lock) updateDeadline() {
	atomic.SwapInt64(&j.dead, time.Now().Add(j.Timeout).UnixNano())
}
func (j *Lock) proceed(condState uint32) {
	if j.updateState(condState) {
		j.updateDeadline()
	}
}
